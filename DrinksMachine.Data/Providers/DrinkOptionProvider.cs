﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DrinksMachine.Data.Models;

namespace DrinksMachine.Data.Providers
{
    /// <summary>
    /// Represents a way of getting details of our drinks options
    /// </summary>
    /// <remarks>
    /// This would probably be interfaced away so that DrinksMachine would never know
    /// the actual provider (that way we could have a generic frontend that supported all sorts of drinks macines)
    /// But since this is a demo we won't go that far!
    /// </remarks>
    public class DrinkOptionProvider
    {
        /// <summary>
        /// Our available drink options
        /// </summary>
        /// <remarks>
        /// In a production system these would be actually loaded from a database.
        /// </remarks>
        private static List<DrinkOption> drinks = new List<DrinkOption>()
        {
            new DrinkOption()
            {
                Id = 0,
                Name = "Lemon Tea",
                Stages = new List<string>()
                {
                    "Boil some water",
                    "Steep the water in the tea",
                    "Pour tea in the cup",
                    "Add lemon"
                }
            },
            new DrinkOption()
            {
                Id = 1,
                Name = "Coffee",
                Stages = new List<string>()
                {
                    "Boil some water",
                    "Brew the coffee grounds",
                    "Pour coffee in the cup",
                    "Add sugar and milk"
                }
            },
            new DrinkOption()
            {
                Id = 2,
                Name = "Chocolate",
                Stages = new List<string>()
                {
                    "Boil some water",
                    "Add drinking chocolate powder to water",
                    "Pour chocolate in the cup"
                }
            }
        };

        /// <summary>
        /// Gets a list of all available drinks
        /// </summary>
        /// <returns>List of DrinkOption objects</returns>
        public List<DrinkOption> List()
        {
            return drinks;
        }

        /// <summary>
        /// Finds the details of a specific drink option, by Id.
        /// </summary>
        /// <param name="id">The Id of the drink option</param>
        /// <returns>A DrinkOption populated with the data from the requested Id, or null if not found.</returns>
        public DrinkOption Read(int id)
        {
            return drinks.Where(m => m.Id == id).FirstOrDefault();
        }
    }
}
