﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DrinksMachine.Data.Models
{
    /// <summary>
    /// Represents a drinks option that could be selected from the machine
    /// </summary>
    public class DrinkOption
    {
        /// <summary>
        /// Unique identifier for reading the information again later.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The displayed name of the drink in the options list.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Stages to be displayed after the drink has been selected.
        /// </summary>
        public List<string> Stages { get; set; }
    }
}
