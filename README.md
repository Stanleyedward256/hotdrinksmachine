# README #

### What is this repository for? ###

A demo of an MVC .NET Core web application that renders a hot drinks machine where users can select an option and see the stages gone through to make the drink.

### How do I get set up? ###

* Clone repository
* Open DrinksMachine.sln in the latest Visual Studio 2019
* Build solution
* If there are any problems the try a Nuget Restore.

### Technologies ###

* .NET Core MVC Web Application
* jQuery, including ajax
* CSS3

### Concepts ###
* Provider pattern
* Modular JS
* Rudiments of Single Responsiblity and an awareness of seperating data layer from presentation (though more could be done to segregate functionality via interfaces).

### External resource used ###

* Google fonts (for fonts, obv.)
* Bulma (for layout and button styles)
* Animate.css (for effects)
* Spinkit (for spinner animation)
* Filter Forge (for texturing)

### Alternative designs considered ###
* A REST API Backend, with a Vue.js frontend.
* A Blazor App.

These were dismissed at this method was faster for demonstration purposes.