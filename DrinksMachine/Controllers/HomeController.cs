﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using DrinksMachine.Data.Models;
using DrinksMachine.Data.Providers;

namespace DrinksMachine.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// The home screen, this loads the options for our drinks
        /// machine and then renders the machine and options in the view.
        /// </summary>
        public IActionResult Index()
        {
            DrinkOptionProvider provider = new DrinkOptionProvider();
            return View(provider.List());
        }

        /// <summary>
        /// Called via Ajax from the client when a user selects an option.
        /// It's purpose is to load the details for the specified drink.
        /// </summary>
        /// <remarks>If you get "404 Drink not found!" in your returned messages that's because the Provider.Read return null</remarks>
        public IActionResult ReadOption(int id)
        {
            DrinkOptionProvider provider = new DrinkOptionProvider();

            DrinkOption model = provider.Read(id);
            
            // In a production system we would want a standardised way
            // of returning errors to the front end that our javascript
            // would know how to handle, but again, for demo purposes this
            // way displays a funny message
            if(model == default)
            {
                model = new DrinkOption()
                {
                    Stages = new List<string>()
                    {
                        "404 Drink not found!",
                        "",
                        "Who made this machine!?"
                    }
                };
            }

            return new JsonResult(model);
        }
    }
}
