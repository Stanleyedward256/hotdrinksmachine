﻿/**
 * DrinksMachine is a single object that we are using to hold all the
 * non-events based code for our little project.
 */
var DrinksMachine = function () {

    let drinksMachine = {};

    /**
     * A parsed jQuery spinner object, which we append to the LCD while the ajax call is happening
     */
    drinksMachine.Spinner = $("<div class='spinner'><div class='cube1'></div><div class='cube2'></div></div>");

    /**
     * Clears off our LCD display
     */
    drinksMachine.ClearLCD = function () {
        // We don't get an error here, if the lcd can't be found, so I haven't added a check.
        $("#vending-lcd").empty();
    };

    /**
     * Appends the provided items to our LCD display.
     * @param {array} messages - An array of messages to show in the LCD
     */
    drinksMachine.PopulateLCD = function (messages = []) {
        let lcd = $("#vending-lcd");
        if (lcd.length === 0) {
            alert("Oh dear! The LCD is missing");
        }
        else {
            // Delay is used to stagger the messages that slide in from the right, it's purpose is purely visual.
            let delay = 0;
            messages.forEach(message => {
                let delayStyle = "style='animation-delay: " + (delay * 200).toString() + "ms'"
                message = (message === "") ? "&nbsp;" : message;
                lcd.append("<p class='animate__animated animate__lightSpeedInRight' " + delayStyle + ">" + message + "</p>")
                delay += 1;
            })
        }
    };

    /**
     * Clears the LCD and displays an animated spinner.
     */
    drinksMachine.ShowSpinner = function () {
        this.ClearLCD();
        let lcd = $("#vending-lcd");
        if (lcd.length === 0) {
            alert("Oh dear! The LCD is missing");
        }
        else {
            lcd.html(this.Spinner);
        }
    };

    /**
     * A wrapper for displaying a generic error message
     * @param {string} message
     */
    drinksMachine.ShowClientError = function (message) {
        this.ClearLCD();
        this.PopulateLCD(["CLIENT ERROR", "", message, "", "Please select a different option"]);
    };

    /**
     * Handles calling the server to load the Steps for a specified drink.
     * It is called from the event handler for the buttons.
     * @param {int} id - The id for the drink to load.
     */
    drinksMachine.LoadOptionSteps = function (id) {
        this.ShowSpinner();
        // This timeout is really only so you can see the spinner
        // because I like little details :)
        // obviously we wouldn't have this in production.
        window.setTimeout(function () {
            $.ajax({
                url: "/Home/ReadOption/" + id.toString(),
                success: (data) => {
                    DrinksMachine.ClearLCD();
                    DrinksMachine.PopulateLCD(data.stages);
                },
                error: () => {
                    DrinksMachine.ClearLCD();
                    DrinksMachine.ShowClientError("The server didn't like your request.");
                }
            });
        }, 1000);
    };

    return drinksMachine;
}();


$(document).ready(function () {
    /**
     * Handles our click event on the buttons
     */
    $(".vending-button").click(function (event) {
        let control = $(event.target);
        // In practice we should never not have a control, but it pays to check.
        if (control.length === 0) {
            DrinksMachine.ShowClientError("The button you clicked isn't working.");
        }
        else {
            let id = control.data("id");
            DrinksMachine.LoadOptionSteps(id);
        }
    });

    DrinksMachine.ClearLCD();
    DrinksMachine.PopulateLCD(["Please select a drink..."]);
});